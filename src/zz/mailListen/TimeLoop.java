package zz.mailListen;

import java.io.IOException;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.mail.MessagingException;

public class TimeLoop {
	private Timer time = new Timer();
	
	public void start(){
		time.schedule((new loopTask()), 0, 1*60*1000);
	}
	
	private class loopTask extends TimerTask{
		@Override
		public void run() {
			// TODO Auto-generated method stub
			MailServer_Down down = new MailServer_Down();
			try {
				down.receive();
				Date date = new Date();
				System.out.println("down" + "------>" + date);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			MailServer_Send send = new MailServer_Send();
			try {
				send.SendShenHe();
				Date date = new Date();
				System.out.println("Send" + "------>" + date);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//这里调用那个createReport,需要考虑的是如果处理word的时间大于2分钟，这个该怎么处理？
			Runtime rn = Runtime.getRuntime();
			Process p = null;
			try {
				p = rn.exec("D:\\浙江省质量检测研究院\\bin\\Debug\\CreateReport.exe");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	public static void main(String[] args) {
		TimeLoop loop = new TimeLoop();
		loop.start();
	}
	
}
