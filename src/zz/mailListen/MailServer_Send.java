package zz.mailListen;

import java.io.File;
import java.io.FileNotFoundException;




public class MailServer_Send {
	private String _userName = "lablistsenddata@163.com";
	private String _userKey = "123456zxcvbnm";

	private String tmp_void_PATH = "D:\\浙江省质量检测研究院\\serverData\\tmp_void";

	private File tmp_void; // 存放tmp_void 的目录，存放那些没有审核人的数据表

	public MailServer_Send() {
		tmp_void = new File(tmp_void_PATH);
		if (!tmp_void.exists()) {
			tmp_void.mkdirs();
		}
	}

	// 用于发送，标识了审核人的邮件；
	public void SendShenHe() throws Exception {
		File[] singleFiles = tmp_void.listFiles();

		for (int i = 0; i < singleFiles.length; i++) {
			Smtp_Mail m = new Smtp_Mail(_userName, _userKey);
			String[] toAdd = { _userName };

			m.setTo(toAdd);
			m.setFrom(_userName);
			m.setBody("This_is_data");
			m.setSubject(changeName(singleFiles[i].getName()));

			File[] datas = singleFiles[i].listFiles();
			for (int j = 0; j < datas.length; j++) {
				m.addAttachment(tmp_void_PATH + "\\" 
						+ singleFiles[i].getName()
						+ "\\" + datas[j].getName(), datas[j].getName());
			}
			if (m.send()) {
				System.out.println("Send Shenhe Success");
				deleteData(singleFiles[i]);
			}else{
				System.out.println("Send Shenhe Fail");
			}

		}
		
	}

	// 将文件夹名中审核人void 的部分改成随机指定的用户；
	private String changeName(String srcString) throws FileNotFoundException {
		String[] tmp = srcString.split("\\$");
		
		//ReturnName shenheName = new ReturnName();
		//不再通过文本文件读取，直接通过数据库筛选人
		tmp[3] = SqlQuery.returnName(tmp[0]);
		
		//change后给数据库里的用户，正在审核（checking+1）
		
		String query = "update user set checking = checking+1 where username = '"+tmp[3]+"'";
		System.out.println(tmp[3]);
		SqlQuery.exeQuery(query);		

		return tmp[0] + "$" + tmp[1] + "$" + tmp[2] + "$" + tmp[3];
	}

	// 用于删除文件夹
	private void deleteData(File dataFolder) {
		File[] childData = dataFolder.listFiles();
		for (int i = 0; i < childData.length; i++) {
			childData[i].delete();
		}
		dataFolder.delete();
	}
	
}
