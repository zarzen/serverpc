package zz.mailListen;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMessage;

public class MailServer_Down {
	private String _serverHost = "pop.163.com";
	private String _userName = "lablistsenddata@163.com";
	private String _userKey = "123456zxcvbnm";
	private String tmp_void_PATH = "D:\\浙江省质量检测研究院\\serverData\\tmp_void";
	private String pass_PATH= "D:\\浙江省质量检测研究院\\serverData\\通过的表单";

	private File tmp_void; // 存放tmp_void 的目录，存放那些没有审核人的数据表
	private File pass; // 通过审核的表格的存放目录

	public MailServer_Down() {
		tmp_void = new File(tmp_void_PATH);
		pass = new File(pass_PATH);
		if (!tmp_void.exists()) {
			tmp_void.mkdirs();
		}
		if (!pass.exists()) {
			pass.mkdirs();
		}

	}

	public void receive() throws MessagingException, IOException {
		Properties props = _setProps();
		Session session = Session.getDefaultInstance(props);

		Store store = session.getStore();
		store.connect(_serverHost, _userName, _userKey);

		Folder folder = store.getFolder("INBOX");
		folder.open(Folder.READ_WRITE);

		Message message[] = folder.getMessages();
		for (int i = 0; i < message.length; i++) {
			if (check_void(message[i].getSubject())) {
				Save(tmp_void_PATH, (MimeMessage)message[i]);
			}else if (check_pass(message[i].getSubject())) {
				Save(pass_PATH, (MimeMessage)message[i]);
				//pass的给审核人，主检工作量都加1，正在审核数-1
				
				String subject=message[i].getSubject();
				String tmp[] = subject.split("\\$");
				String zhujian = tmp[0];
				String shenhe = tmp[3];
				
				//主检pass +1
				String zhujianQuery = "update user set passed = passed+1 where username = '"+zhujian+"'";
				SqlQuery.exeQuery(zhujianQuery);
				
				//审核 checked+1；checking-1
				String shenheQueryChecked = "update user set checked = checked+1 where username = '"+shenhe+"'";
				SqlQuery.exeQuery(shenheQueryChecked);
				String shenheQueryChecking = "update user set checking = checking-1 where username = '"+shenhe+"'";
				SqlQuery.exeQuery(shenheQueryChecking);
				
			}else {
			}
		}

		folder.close(true);
		store.close();
	}

	// 判断mail是否通过tmp_void 的判断,返回true表示需要 存回本地，指定审核人
	private Boolean check_void(String subject) {
		String tmp[] = subject.split("\\$");
		if (tmp.length == 4) {
			if (tmp[3].equals("void")) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}

	}
	//判断mailSubject，是否通过是pass
	
	private Boolean check_pass(String subject){
		String[] tmp = subject.split("\\$");
		if (tmp.length == 4) {
			if (tmp[2].equals("pass")) {
				return true;
			}else{return false;}
		}else{
			return false;
		}
	}
	// 设置props
	private Properties _setProps() {
		Properties props = new Properties();
		props.setProperty("mail.pop3.host", _serverHost);
		props.setProperty("mail.store.protocol", "pop3");

		return props;
	}

	// 遇到需要保存的文件时，调用save函数。传入相应的数据目录；
	private void Save(String savePath , MimeMessage msg) throws MessagingException, IOException {
		String _PATH = savePath + "\\" + msg.getSubject();
		File singleData = new File(_PATH);
		if (!singleData.exists()) {
			singleData.mkdirs();
		}
		
		Multipart mp = (Multipart)msg.getContent();
		for (int i = 0; i < mp.getCount(); i++) {
			BodyPart mpart = mp.getBodyPart(i);
			String disposition = mpart.getDisposition();
			if ((disposition != null)
					&& ((disposition.equals(Part.ATTACHMENT)) || (disposition
							.equals(Part.INLINE)))) {
				File storeFile = new File(_PATH + "\\" + mpart.getFileName());
				BufferedOutputStream bos = null;  
		        BufferedInputStream bis = null;  
		        bos = new BufferedOutputStream(new FileOutputStream(storeFile));  
	            bis = new BufferedInputStream(mpart.getInputStream());  
	            int c;  
	            while((c= bis.read())!=-1){  
	                bos.write(c);  
	                bos.flush();  
	            }  
	            bos.close();
	            bis.close();

			}
		}
		//设置邮件删除标记
		msg.setFlag(Flags.Flag.DELETED, true);

	}


}
